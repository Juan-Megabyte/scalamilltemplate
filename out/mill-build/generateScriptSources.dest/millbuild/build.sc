package millbuild

import _root_.mill.runner.MillBuildRootModule

object MiscInfo_build {
  implicit val millBuildRootModuleInfo: _root_.mill.runner.MillBuildRootModule.Info = _root_.mill.runner.MillBuildRootModule.Info(
    Vector("/home/rusticd/.cache/mill/download/0.11.1", "/home/rusticd/Documents/Programming/Scala/scalamilltemplate/out/mill-launcher/0.11.1.jar").map(_root_.os.Path(_)),
    _root_.os.Path("/home/rusticd/Documents/Programming/Scala/scalamilltemplate"),
    _root_.os.Path("/home/rusticd/Documents/Programming/Scala/scalamilltemplate"),
    _root_.scala.Seq("com.lihaoyi::mill-contrib-bloop:")
  )
  implicit val millBaseModuleInfo: _root_.mill.main.RootModule.Info = _root_.mill.main.RootModule.Info(
    millBuildRootModuleInfo.projectRoot,
    _root_.mill.define.Discover[build]
  )
}
import MiscInfo_build.{millBuildRootModuleInfo, millBaseModuleInfo}
object build extends build
class build extends _root_.mill.main.RootModule {

//MILL_ORIGINAL_FILE_PATH=/home/rusticd/Documents/Programming/Scala/scalamilltemplate/build.sc
//MILL_USER_CODE_START_MARKER
import mill._
import mill.scalalib._

object scalamilltemplate extends ScalaModule {

  def scalaVersion = "3.3.0"

  object test extends ScalaTests with TestModule.Munit {
    def ivyDeps = Agg(
      ivy"org.scalameta::munit::0.7.29"
    )
  }
}


}